from django import forms 

from croppie.fields import CroppieField

from pets.models import Photo


class UploadPhotoForm(forms.ModelForm):
    photo = CroppieField(options={

        'viewport': { 'width': 100, 'height': 100 },
        'boundary': { 'width': 300, 'height': 300 },
        'showZoomer': False,
        'enableResize': False,
        'enableOrientation': True,})
    nickname = forms.CharField()
    name = forms.CharField()


    class Meta:
        model = Photo
        exclude =('pet',)
