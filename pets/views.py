from django.contrib import messages
from django.shortcuts import get_object_or_404
from django.views.generic import CreateView, RedirectView, TemplateView
from django.urls import reverse_lazy

from  pets.forms import UploadPhotoForm
from pets.models import Pet, Photo


class HomeView(TemplateView):
    template_name = 'home.html'
    def get_context_data(self):
        ctx = super().get_context_data()
        ctx.update({'photos': Photo.objects.all()})
        return ctx


class UploadView(CreateView):
    template_name = 'upload.html'
    form_class = UploadPhotoForm
    success_url = reverse_lazy('home')

    def form_valid(self, form):
        pet, created = Pet.objects.get_or_create(
                nickname = form.cleaned_data['nickname'],
                name = form.cleaned_data['name'])
        form.instance.pet = pet
        return super().form_valid(form)
    

class VoteView(RedirectView):
    url = reverse_lazy('home') 
    def get(self, request, pet_id, *args, **kwargs):
        voted = request.session.get('voted', False)
        if voted is False:
            pet = get_object_or_404(Pet, id=pet_id)
            pet.votes = pet.votes + 1
            pet.save()
            request.session['voted'] = pet.id
            messages.add_message(request, messages.INFO, 'You have voted')
        else:
            messages.add_message(request, messages.ERROR, 'You can not vote more than once')

        return super().get(request, *args, **kwargs)
