from django.db import models

class Pet(models.Model):
    name = models.CharField(max_length=100)
    nickname = models.CharField(max_length=100)
    votes = models.IntegerField(default=0)

class Photo(models.Model):
    photo = models.ImageField(upload_to='upload')
    pet = models.ForeignKey(
            Pet,
            related_name='photos',
            on_delete=models.CASCADE)



